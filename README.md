# PE agreement template

minimalistic risk-free template for agreement between software development company and programmer acting as private entrepreneur, licensed as CC0 (https://creativecommons.org/publicdomain/zero/1.0/legalcode)